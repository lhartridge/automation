﻿using Automation.Helpers;
using Automation.Helpers.PageConfiguration;
using Automation.Helpers.User;
using Automation.UIElements.Chunks.Index;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.UIElements
{
    /// <summary>
    /// This class is the Parent for most Pages
    /// Page: UI Element that contains all the Page, all the element inside a Page
    /// are considered chunks
    /// </summary>
    public abstract class AbstractPage : UIElement
    {
        protected IWebDriver driver;
        protected WebElementQueryHelper queryHelper;
        protected AjaxHelper ajaxHelper;
        protected PageConfigurationHelper pageConfigurationHelper;
        
        protected AbstractPage(IWebDriver driver)
        {
            this.driver = driver;
            queryHelper = new WebElementQueryHelper(driver);
            this.ajaxHelper = AjaxHelper.getAjaxHelper(driver);            
        }

        /// <summary>
        /// This Constructors allows to override default user
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="context"></param>
        /// <param name="user"></param>
        public AbstractPage(IWebDriver driver, IUser user)
        : this(driver)
        {

            if (user != null)
            {
                //Set User here
            }
            
        }

        /// <summary>
        /// This Constructors allows to select the File
        /// </summary>
        /// <param name="driver"></param>
        /// <param name="context"></param>
        /// <param name="user"></param>
        protected AbstractPage(IWebDriver driver, string fileName){
            this.driver = driver;
            this.pageConfigurationHelper = new PageConfigurationHelper(fileName);
            queryHelper = new WebElementQueryHelper(driver);
            this.ajaxHelper = AjaxHelper.getAjaxHelper(driver);
        }

        /// <summary>
        /// A page must implement this method to perform step-by-step in order to access to the page
        /// </summary>
        public abstract void navigate();

        public override IElement getParent()
        {
            return null;
        }

        public override IWebDriver getDriver()
        {
            return driver;
        }

        public override string getSelector()
        {
            return "";
        }

        
        public void reload()
        {
            driver.Navigate().Refresh();
        }

        public override PageConfigurationHelper getPageConfigurationHelper()
        {
            return this.pageConfigurationHelper;
        }

    }
}
