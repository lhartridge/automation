﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.PageObject
{
    public class LoginOptimizedPage
    {
        private IWebDriver driver;

        // A diferencia de LoginPage, hemos creado un constructor que recibe el Driver
        // y directamente inicia el PageFactory sin necesitar de repetir este codigo
        // cada vez que necesitemos de este Page Object
        public LoginOptimizedPage(IWebDriver driver)
        {
            this.driver = driver;
            PageFactory.InitElements(driver, this);
        }

        [FindsBy(How = How.CssSelector, Using = ".LoginDialog-content.modal-content")]
        public IWebElement loginModal { get; set; }

        [FindsBy(How = How.CssSelector, Using = ".text-input.email-input.js-signin-email")]
        public IWebElement email { get; set; }

        [FindsBy(How = How.CssSelector, Using = ".LoginForm-input.LoginForm-password .text-input")]
        public IWebElement password { get; set; }

        [FindsBy(How = How.CssSelector, Using = ".submit.btn.primary-btn.js-submit")]
        public IWebElement btnSubmit { get; set; }
    }
}
