﻿using Automation.Helpers;
using Automation.Helpers.PageConfiguration;
using Automation.Helpers.User;
using Automation.UIElements.Chunks.Index;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.UIElements.Pages
{
    public class Mg2IndexPage : AbstractPage
    {
        private static string fileName = "IndexUIMap";
        private static string emailInput = "emailInput";
        public Mg2IndexPage(IWebDriver driver) : base(driver, fileName) {}

        public override void navigate()
        {
            throw new NotImplementedException();
        }

        public IWebElement getEmailInput()
        {
            String selector = PageConfiguration.getSelector(emailInput, getPageConfigurationHelper().myPageConfiguration);
             var element = queryHelper.findElementByCss(selector);
             return element;
        }
    }
}
