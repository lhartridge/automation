﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.UIElements.Pages.Welcome
{
    public class WelcomePage : AbstractPage
    {
        //Properties File
        private static string propertiesFile = "WelcomeUIMap";

        //
        private static string loginButton = "a.Button.StreamsLogin.js-login";
        public WelcomePage(IWebDriver driver) : base(driver, propertiesFile) { }

        public override void navigate()
        {
            throw new NotImplementedException();
        }
    }
}
