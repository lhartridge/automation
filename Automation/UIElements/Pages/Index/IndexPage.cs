﻿using Automation.UIElements.Chunks.Index;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automation.Helpers;
using OpenQA.Selenium;
using Automation.Helpers.User;


namespace Automation.UIElements.Pages
{
    public class IndexPage : AbstractPage
    {
        //Properties File
        private static string propertiesFile = "IndexUIMap";

        //Locators Key

        //Constructors
        public IndexPage(IWebDriver driver, IUser user) : base(driver, user) { }

        public IndexPage(IWebDriver driver) : base(driver, propertiesFile) { }

        // Methods and Functions
        public TrendingTopic goToTrendingTopic()
        {
            TrendingTopic tp = new TrendingTopic(this);
            ajaxHelper.waitForElementForNoTimeout(tp, true, 3);
            return tp;

        }
        public override void navigate()
        {
            
        }
    }
}
