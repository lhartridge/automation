﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.PageObjects;
using System.Configuration;

namespace Automation.PageObject
{
    public class WelcomePage
    {
        //private IWebDriver driver;

        [FindsBy(How = How.CssSelector, Using = "a.Button.StreamsLogin.js-login")]
        [CacheLookup]
        public IWebElement logInButton { get; set; }
    }
}
