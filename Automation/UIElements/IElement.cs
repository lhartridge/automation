﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using Automation.Helpers;
using Automation.Helpers.PageConfiguration;


namespace Automation.UIElements
{
    /*
     * IElement: Es la interfaz base para todos los Page Object en el framework de Automation
     * Implementa el Patron de Diseño Composite, es decir que el IElement es un single
     * parent y posee numerosos IElement hijos
     */
    public interface IElement
    {
        PageConfigurationHelper getPageConfigurationHelper();
        IWebDriver getDriver();

        ISet<IElement> getChildren();

        IElement getParent();

        void addChild(IElement child);

        String getSelector();

        String getAbsoluteSelector();

    }
}
