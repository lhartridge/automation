﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automation.Helpers.PageConfiguration;

namespace Automation.UIElements
{
    /*
     * Abstract Class that implements IElement
     * Parent Class for AbstractPage and AbstractPageChunk
     */
    public abstract class UIElement : IElement
    {
        
        public HashSet<IElement> children = new HashSet<IElement>();

        public HashSet<IElement> getChildren(){return children;}

        public void addChild(IElement child) { children.Add(child);}

        /// <summary>
        /// Calls doGetAbsoluteSelector
        /// </summary>
        /// <returns>Abosolute String Selector</returns>
        public string getAbsoluteSelector()
        {
            return doGetAbsoluteSelector(this);
        }

        /// <summary>
        /// Get the Absolute Selecter for IElement
        /// </summary>
        /// <param name="chunk">IElement</param>
        /// <returns>Absolute String Selector</returns>
        private String doGetAbsoluteSelector(IElement chunk)
        {
            String result = chunk.getSelector();
            IElement parent = chunk.getParent();

            if (parent != null)
            {
                String prefix = doGetAbsoluteSelector(parent);
                if (prefix != null) 
                {
                    result = prefix + " " + result;
                }
            }

            return result;
        }

        ISet<IElement> IElement.getChildren()
        {
            throw new NotImplementedException();
        }

        public virtual IElement getParent()
        {
            return null;
        }

        public virtual string getSelector()
        {
            throw new NotImplementedException();
        }

        public virtual IWebDriver getDriver()
        {
            throw new NotImplementedException();
        }

        public virtual PageConfigurationHelper getPageConfigurationHelper()
        {
            throw new NotImplementedException();
        }
    }
}
