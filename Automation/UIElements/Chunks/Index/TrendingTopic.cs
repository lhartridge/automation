﻿using Automation.Helpers.PageConfiguration;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.UIElements.Chunks.Index
{
    public class TrendingTopic : AbstractPageChunk
    {
        private static string trendItemListContainer = ".Trends.module.trends";
        private static string trendItemList = "trendItemList";
        

        public TrendingTopic(IElement parent) : base(parent,trendItemListContainer) { }


        public List<IWebElement> getListOfTrendingTopics()
        {
            String cssSelector = PageConfiguration.getSelector(trendItemList, myPageConfigurationHelper.myPageConfiguration);
            //String cssSelector = ".trend-item.js-trend-item";
            //String co = getContext().getProperty("asd", "asd", trendItemList);
            //String cssSelectorUsingJQuery = getContext().getProperty(trendItemList);

            var elements = queryHelper.findElementsByCss(cssSelector);
            return elements;
        }
    }
}
