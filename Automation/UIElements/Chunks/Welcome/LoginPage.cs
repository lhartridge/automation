﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using Automation.UIElements;

namespace Automation.PageObject
{
    public class LoginPage : AbstractPageChunk
    {
        //Properties File
        private static string propertiesFile = "IndexUIMap";

        //Locators Key


        public LoginPage(IElement parent) : base(parent,".LoginDialog-content.modal-content") { }

    }
}
