﻿using Automation.Helpers;
using Automation.Helpers.PageConfiguration;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.UIElements
{
    /// <summary>
    /// Extends from Abstract Class UIElement
    /// This class is parent of most of the elements on the page such as tables, diagrams, etc;
    /// </summary>
    public abstract class AbstractPageChunk : UIElement
    {
        protected IElement parent;
        protected String selector;
        protected WebElementQueryHelper queryHelper;
        protected AjaxHelper ajaxHelper;
        public static TimeSpan defaultTimeout = TimeSpan.FromSeconds(45);
        protected PageConfigurationHelper myPageConfigurationHelper;

        protected AbstractPageChunk(IElement parent, String selector)
        {
            this.parent = parent;
            this.selector = selector;
            IWebDriver driver = parent.getDriver();
            this.queryHelper = new WebElementQueryHelper(driver);
            this.ajaxHelper = AjaxHelper.getAjaxHelper(driver);
            this.myPageConfigurationHelper = parent.getPageConfigurationHelper();
            parent.addChild(this);
        }

        public override IWebDriver getDriver()
        {
            return parent.getDriver();
        }

        public override IElement getParent()
        {
            return parent;
        }

        public override string getSelector()
        {
            return selector;
        }

        public override PageConfigurationHelper getPageConfigurationHelper()
        {
            return parent.getPageConfigurationHelper();
        }

        public IWebElement getWebElement()
        {
            AbstractPageChunk chunk = this;
            WebDriverWait wait = new WebDriverWait(getDriver(),defaultTimeout);
            IWebElement webElement = getWebElementInmediatly(chunk);
            if (webElement != null && webElement.Displayed)
            {
                return webElement;
            }
            return null;
        }

        private IWebElement getWebElementInmediatly(AbstractPageChunk chunk)
        {
            try
            {
                return queryHelper.findElement(chunk);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                ajaxHelper.suspend(200);
                IWebElement element = queryHelper.findElement(chunk);
                return element;
            }
        }

        public IWebElement getVisibleWebElement(int timeout)
        {
            WebDriverWait wait = new WebDriverWait(getDriver(), TimeSpan.FromSeconds(timeout));
            AbstractPageChunk chunk = this;
            IWebElement webElement = getWebElementInmediatly(chunk);
            if (webElement != null && webElement.Displayed)
            {
                return webElement;
            }
            return null;
        }

        public void focus()
        {
            ((IJavaScriptExecutor)getDriver()).ExecuteScript("$('" + getAbsoluteSelector() + "').focus()");
        }

        /**
 * Click the element.  Details: Wait for the element, try to click it, onError attempt to scroll element to bottom of page and click, if still onError try to scroll element to top of page and click.
 */
        public void click()
        {
            // First wait for the element
            IWebElement element = getVisibleWebElement(defaultTimeout.Seconds);
            try
            {
                // Attempt to click the element
                element.Click();
            }
            catch (WebDriverException ex)
            {
                try
                {
                    scrollIntoView(false);
                    ajaxHelper.suspend(1000);
                    getVisibleWebElement(5).Click();
                }
                catch (WebDriverException e)
                {
                    try
                    {
                        Console.WriteLine(e);
                        // If clicking fails after scrolling to page bottom, scroll to page top and try again.
                        //logger.warn("Element still not clickable, going to scroll to element (at page top). Error: " + ex);
                        scrollIntoView(true);
                        ajaxHelper.suspend(1000);
                        getVisibleWebElement(5).Click();
                    }
                    catch (WebDriverException e2)
                    {
                        // Giving up!
                        //logger.error("Element still not clickable. Error: " + ex);
                        Console.WriteLine(e2);
                        throw ex;
                    }
                }
            }
        }

        /**
         * With new UI Chromedriver can't scroll to elements with hidden overflow:
         * Defect is here: https://code.google.com/p/chromedriver/issues/detail?id=720
         * Scroll into view via jQuery
         * @author mkhimich
         */
        public void scrollIntoView()
        {
            scrollIntoView(false);
        }

        /**
         * Scroll into view with option to decide top or bottom alignment.
         * @param alignToTopElseBottom
         */
        public void scrollIntoView(bool alignToTopElseBottom)
        {
            try
            {
                ((IJavaScriptExecutor)getDriver()).ExecuteScript("$('" + getAbsoluteSelector() + "')[0].scrollIntoView(" + alignToTopElseBottom + ")");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                // Some elements cannot be scrolled, so ignore this.
            }
        }

        /**
         * Scroll to bottom
         */
        public void scrollToBottom()
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)parent.getDriver();
            String query = "jQuery(jQuery.find('" + this.getAbsoluteSelector() + "')).scrollTop(99999)";
            //logger.info("Scroll to bottom via JS");
            //logger.info(query);
            js.ExecuteScript(query);
        }

    }
}
