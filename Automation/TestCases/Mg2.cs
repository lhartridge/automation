﻿using Automation.Helpers.User;
using Automation.UIElements.Pages;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Automation.UIElements.Chunks.Index;

namespace Automation.TestCases
{
    public class Mg2 : BaseWebDriverTest
    {

        [Test]
        public void testMG2()
        {
            driver.Navigate().GoToUrl("https://dev-mg2group.subscriberconcierge.com/Home");
            Mg2IndexPage indexPage = new Mg2IndexPage(driver);
            ajaxHelper.suspend(200);

              indexPage.getEmailInput().SendKeys("COSOMOJO");
        }
    }
}
