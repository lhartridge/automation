﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using NUnit.Framework;
using Automation.PageObject;
using OpenQA.Selenium.Support.PageObjects;
using System.Configuration;
using Automation.UIElements.Chunks.Index;
using Automation.UIElements.Pages;
using Automation.Helpers.User;

namespace Automation.TestCases
{
    public class Twitter : BaseWebDriverTest
    {

        [Test]
        public void lanzarTwitter()
        {
            // Agregar Referencia de System.Configuration en References para agregar el ConfigurationManager
            //Recordar que para hacer configuraciones externas el ConnectionString no debe contener ninguna etiqueta xml extra
            //solo debe contener el connection string y agregar la carpeta Configuration en la referencia
            var dbString = ConfigurationManager.ConnectionStrings["localhost"].ConnectionString;

            // Creating Index Page Instance
            var indexPage = new WelcomePage();
            PageFactory.InitElements(base.driver, indexPage);

            // Click on Login Button
            indexPage.logInButton.Click();
            base.driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(60));
         }

        [Test]
        public void testVerifyTrendingTopics()
        {
            // Welocome Page should be Updated with the UI Concept
            var welcomePage = new WelcomePage();
            PageFactory.InitElements(driver, welcomePage);

            // Click on Login Button
            welcomePage.logInButton.Click();
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(60));
            
            //Creating Log In Instance
            IndexPage indexPage = new IndexPage(driver);
        }

        [Test]
        public void loginOnTwitter()
        {
            // Creating Index Page Instance
            var welcomePage = new WelcomePage();
            PageFactory.InitElements(driver, welcomePage);

            // Click on Login Button
            welcomePage.logInButton.Click();
            driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(60));

            // Creating Login Page Instance
            var loginPage = new LoginOptimizedPage(driver);
            PageFactory.InitElements(driver, loginPage);

            // Verify that Log in Modal View is shown
            loginPage.email.Clear();
            loginPage.email.SendKeys("lhartridgeVM");
            loginPage.password.Clear();
            loginPage.password.SendKeys("automation");
            loginPage.btnSubmit.Click();

            IndexPage indexPage = new IndexPage(driver);
            ajaxHelper.suspend(200);
            var trendingTopic = indexPage.goToTrendingTopic();

            List<IWebElement> elements = trendingTopic.getListOfTrendingTopics();
            foreach (var item in elements)
            {
                Console.WriteLine(item.Text);
            }
            
        }

    }
}
