﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;

namespace Automation.Helpers.Driver
{
    public class WebDriverWrapper
    {
        //Metodo que devuelve la instancia del Browser Requerido
        public static IWebDriver getBrowser(Driver.Drivers searchedDriver)
        {
            IWebDriver driver = null;
            switch (searchedDriver)
            {
                case Driver.Drivers.Firefox:
                    driver = new FirefoxDriver();
                   return driver;
                case Driver.Drivers.Chrome:
                   ChromeOptions options = new ChromeOptions();
                   options.AddArguments("--test-type");
                   driver = new ChromeDriver("C:\\Users\\lucas\\Desktop\\Automation\\Automation\\Automation\\Drivers", options);
                    return driver;
                    //Driver C
                default:
                    driver = new InternetExplorerDriver();
                    return driver;
            }
        }

        //Todos los Browsers deben ser cerrados, de otra manera permaneceran las instancias activas en memoria
        public static void closeBrowser(IWebDriver driver){
            try
            {
                driver.Quit();
            }
            catch (Exception e)
            {
                Console.Write(e.ToString());
            }
        }
    }
}
