﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Helpers.User
{
    public class CustomUser : IUser
    {
        private String login;
        private String password;

        public CustomUser(String login, String password)
        {
            this.login = login;
            this.password = password;
        }

        public string getLogin()
        {
            return login;
        }

        public string getPassword()
        {
            return password;
        }
    }
}
