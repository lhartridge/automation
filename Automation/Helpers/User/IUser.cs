﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Helpers.User
{
    /// <summary>
    /// Interface for all the type of User in the App to Set login and Password
    /// </summary>
    public interface IUser
    {
        String getLogin();
        String getPassword();
    }
}
