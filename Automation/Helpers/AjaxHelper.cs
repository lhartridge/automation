﻿using Automation.UIElements;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Automation.Helpers
{
    public class AjaxHelper
    {
        private static Hashtable helpersPerDriver = new Hashtable();
        private WebElementQueryHelper queryHelper;
        private IWebDriver driver;
        private AjaxHelper(IWebDriver driver)
        {
            queryHelper = new WebElementQueryHelper(driver);
            this.driver = driver;
        }

        /// <summary>
        /// Returns a Singleton Instance of Ajax Helpers
        /// If no Ajax Helper exist in current driver, a new one is created 
        /// </summary>
        /// <param name="driver"></param>
        /// <returns></returns>
        public static AjaxHelper getAjaxHelper(IWebDriver driver)
        {
            if (helpersPerDriver.ContainsKey(driver))
            {
                return (AjaxHelper)helpersPerDriver[driver];
            }
            else
            {
                AjaxHelper newHelper = new AjaxHelper(driver);
                helpersPerDriver.Add(driver, newHelper);
                return newHelper;
            }
        }
        /// <summary>
        /// Method sleeps the thread for X miliseconds
        /// </summary>
        /// <param name="millis"></param>
        public void suspend(int millis)
        {
            try
            {
                Thread.Sleep(millis);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        /// <summary>
        /// If appear = true. Wait for Element to Appear
        /// If appear = false. Wait for Element to Dissapear
        /// </summary>
        /// <param name="element"></param>
        /// <param name="appear"></param>
        /// <param name="secondsToWait"></param>
        public void waitForElementForNoTimeout(IElement element, bool appear, int secondsToWait)
        {
            String absoluteSelector = element.getAbsoluteSelector();
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(secondsToWait));
            bool isPresent = queryHelper.isElementPresent(absoluteSelector.Trim());
        }
    }
}
