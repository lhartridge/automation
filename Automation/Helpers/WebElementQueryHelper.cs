﻿using Automation.UIElements;
using OpenQA.Selenium;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Helpers
{
    public class WebElementQueryHelper
    {
        //Private Params
        private IJavaScriptExecutor driverConsole;
        private static String NoJqueryFailSafe = "if(typeof jQuery == 'undefined') return null; ";


        /// <summary>
        /// Constructor that Initiates a new web element query helper
        /// </summary>
        /// <param name="webDriver"></param>
        public WebElementQueryHelper(IWebDriver webDriver)
        {
            driverConsole = (IJavaScriptExecutor)webDriver;
        }

        /// <summary>
        /// Find element by AbstractPageChunk
        /// </summary>
        /// <param name="chunk"></param>
        /// <returns> web element or null if nothing is found</returns>
        public IWebElement findElement(AbstractPageChunk chunk)
        {
            String selector = chunk.getAbsoluteSelector();
            return findElementByCss(selector);
        }

        /// <summary>
        /// Returns the First Element if matches
        /// </summary>
        /// <param name="cssSelector"></param>
        /// <returns></returns>
        public IWebElement findElementByCss(String cssSelector)
        {
            List<IWebElement> elements = findElementsByCss(cssSelector);
            if (elements != null && elements.Count() > 0)
            {
                return elements[0];
            }
            return null;
        }

        /// <summary>
        /// Returns a list of Web Elements searched by Jquery
        /// </summary>
        /// <param name="cssSelector"></param>
        /// <returns></returns>
        public List<IWebElement> findElementsByCss(String cssSelector)
        {
            String javaScriptExpression = createSelectorExpression(cssSelector);
            try
            {
                List<IWebElement> webElement = new List<IWebElement>();
                var elements = driverConsole.ExecuteScript(javaScriptExpression);
                foreach (var item in (IEnumerable)elements)
                {
                    webElement.Add((IWebElement)item);
                }
                return webElement;
            }
            catch (Exception e)
            {
                Console.WriteLine("Selector: {0} not found. Exception thrown.", cssSelector, e);
            }

            return new List<IWebElement>();
        }

        /// <summary>
        /// Returns the Query to search in console
        /// </summary>
        /// <param name="selector"></param>
        /// <returns></returns>
        private String createSelectorExpression(String selector)
        {
            return NoJqueryFailSafe + "return jQuery.find('" + selector.Trim() + "')";
        }

        /// <summary>
        /// Is the Element currently present or not ?
        /// Checks if element is present
        /// </summary>
        /// <param name="selector"></param>
        /// <returns>True is element is present </returns>
        public bool isElementPresent(string selector)
        {
            String script = NoJqueryFailSafe + "return jQuery.find('" + selector.Trim() + ":visible')[0] != undefined";
            Object result = driverConsole.ExecuteScript(script);
            if (result == null)
            {
                return false;
            }

            return true;
        }
    }
}
