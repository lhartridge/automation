﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Helpers.PageConfiguration
{
    public class RootObject
    {
        public PageConfiguration PageConfiguration { get; set; }
    }
}
