﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Helpers.PageConfiguration
{
    public class PageConfiguration
    {
        private static string _selector;
        public string PageName { get; set; }
        public string URL { get; set; }
        public List<Child> Childs { get; set; }

        /// <summary>
        /// Returns the Selector using a Key and a pageConfiguration (Json File)
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="pageConfiguration"></param>
        /// <returns></returns>
        public static String getSelector(string Key, PageConfiguration pageConfiguration)
        {
            return findElementByKey(pageConfiguration.Childs, Key);
        }

        /// <summary>
        /// Function that Returns the Searched Key
        /// </summary>
        /// <param name="uiElements"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        private static string findElementByKey(List<Child> uiElements, string key)
        {
            List<Child> uiChildElements = new List<Child>();

            foreach (var uiElement in uiElements)
            {
                if (uiElement.key == key)
                {
                    _selector = uiElement.selector;
                    return _selector;
                }
                    //Busco si el Nodo en el Que me encuentro posee Hijos
                else if (uiElement.Childs.Count > 0)
                {
                    //Si tiene Hijos los agrego a mi nueva lista
                    uiElement.Childs.ForEach(item => uiChildElements.Add(item));
                }  
            }

            // Evaluo si mi lista de Childrens posee hijos
            if (uiChildElements.Count() > 0)
            {
               return findElementByKey(uiChildElements, key);
            }

            return _selector;
        }
    }
}
