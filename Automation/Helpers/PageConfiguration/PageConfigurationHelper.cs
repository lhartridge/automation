﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Helpers.PageConfiguration
{
    public class PageConfigurationHelper
    {
        public PageConfigurationHelper(String jsonFileName)
        {
            //Open Json File
            // Load in Memory
            //Texto en formato json dentro del streamreader
            var baseDirectory = "C:\\Users\\lucas\\Source\\Repos\\Automation\\Automation\\UIElements\\UIMap\\";
            var path = baseDirectory + jsonFileName + ".json";

            
            System.IO.StreamReader sr = new System.IO.StreamReader(path);
            string jsonToString = sr.ReadToEnd();
            var rootObject = Newtonsoft.Json.JsonConvert.DeserializeObject<RootObject>(jsonToString);
            myPageConfiguration = rootObject.PageConfiguration;
        }

        public PageConfiguration myPageConfiguration;

    }
}
