﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automation.Helpers.PageConfiguration
{
    public class Child
    {
        public string type { get; set; }
        public string key { get; set; }
        public string selector { get; set; }
        public List<Child> Childs { get; set; }
    }
}
