﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using NUnit.Framework;
using Automation.Helpers;
using Automation.UIElements.Chunks.Index;
using Automation.Helpers.Driver;

namespace Automation
{
    /*
     * BaseWebDriverTest es el padre de todas las clases de test. 
     * Todos los Test de NUnit deben extender de esta clase
     * Esta clase provee funcionalidades basicas de un test para sus descendientes
    */

    public abstract class BaseWebDriverTest
    {
        protected IWebDriver driver = null;
        protected AjaxHelper ajaxHelper;

        [SetUp]
        public void testSetUp()
        {
           driver = WebDriverWrapper.getBrowser(Driver.Drivers.Chrome);
           ajaxHelper = AjaxHelper.getAjaxHelper(driver);
           driver.Manage().Window.Maximize();
           driver.Navigate().GoToUrl("https://twitter.com/?lang=es"); ;
           driver.Manage().Timeouts().ImplicitlyWait(TimeSpan.FromSeconds(10));
           
        }

        [TearDown]
        public void testTearDown()
        {
            WebDriverWrapper.closeBrowser(driver);
        }
    }
}
